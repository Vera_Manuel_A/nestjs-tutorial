import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as pactum from 'pactum';
import { PrismaService } from '../src/prisma/prisma.service';
import { AppModule } from '../src/app.module';
import { AuthDto } from 'src/auth/dto';
import { EditUserDto } from 'src/user/dto';
import { CreateBookmarkDto } from 'src/bookmark/dto';

describe('App e2e', () => {
  let app: INestApplication;
  let prisma: PrismaService;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      })
    );
    await app.init();
    await app.listen(8080);
    prisma = app.get(PrismaService);

    await prisma.cleanDb();
    pactum.request.setBaseUrl('http://localhost:8080');
  });
  afterAll(() => {
    app.close();
  });
  describe('Auth', () => {
    const user: AuthDto = {
      email: 'manuel@manuel.com',
      password: '1234',
    };
    describe('Signup', () => {
      it('should throw if email empty', () => {
        return pactum.spec().post('/auth/signup').withBody({ password: user.password }).expectStatus(400);
      });

      it('should throw if password empty', () => {
        return pactum.spec().post('/auth/signup').withBody({ email: user.email }).expectStatus(400);
      });

      it('should throw if no body provided', () => {
        return pactum.spec().post('/auth/signup').withBody({}).expectStatus(400);
      });

      it('should signup', () => {
        return pactum.spec().post('/auth/signup').withBody(user).expectStatus(201);
      });
    });

    describe('Signin', () => {
      it('should throw if email empty', () => {
        return pactum.spec().post('/auth/signin').withBody({ password: user.password }).expectStatus(400);
      });

      it('should throw if password empty', () => {
        return pactum.spec().post('/auth/signin').withBody({ email: user.email }).expectStatus(400);
      });

      it('should throw if no body provided', () => {
        return pactum.spec().post('/auth/signin').withBody({}).expectStatus(400);
      });

      it('should signin', () => {
        return pactum.spec().post('/auth/signin').withBody(user).expectStatus(200).stores('token', 'access_token');
      });
    });
  });

  describe('User', () => {
    describe('Get me', () => {
      it('should get current user', () => {
        return pactum
          .spec()
          .get('/users/me')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(200);
      });
    });

    describe('Edit User', () => {
      it('should edit user', () => {
        const userEdit: EditUserDto = {
          firstName: 'Manuel',
          lastname: 'Vera',
        };

        return pactum
          .spec()
          .patch('/users')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody(userEdit)
          .expectStatus(200)
          .expectBodyContains(userEdit.firstName)
          .expectBodyContains(userEdit.lastname);
      });
    });
  });

  describe('Bookmarks', () => {
    const bookmark: CreateBookmarkDto = {
      title: 'Titulo Prueba',
      description: 'Book description',
      link: 'www.bookmark.com',
    };
    describe('Get empty Bookmarks', () => {
      it('should get bookmarks', () => {
        return pactum
          .spec()
          .get('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(200)
          .expectBody([]);
      });
    });

    describe('Create Bookmark', () => {
      it('should throw if title empty', () => {
        return pactum
          .spec()
          .post('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody({ link: bookmark.link })
          .expectStatus(400);
      });

      it('should throw if link empty', () => {
        return pactum
          .spec()
          .post('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody({ title: bookmark.title })
          .expectStatus(400);
      });

      it('should throw if no body provided', () => {
        return pactum
          .spec()
          .post('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody({})
          .expectStatus(400);
      });

      it('should create bookmark', () => {
        return pactum
          .spec()
          .post('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody(bookmark)
          .expectStatus(201)
          .stores('bookmarkId', 'id');
      });
    });

    describe('Get Bookmarks', () => {
      it('should get bookmarks', () => {
        return pactum
          .spec()
          .get('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(200)
          .expectJsonLength(1);
      });
    });

    describe('Get bookmark by id', () => {
      it('should get bookmark', () => {
        return pactum
          .spec()
          .get('/bookmarks/{id}')
          .withPathParams('id', '$S{bookmarkId}')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(200)
          .expectBodyContains('$S{bookmarkId}');
      });
    });

    describe('Edit Bookmark', () => {
      const editBookmark = {
        link: 'https://www.youtube.com/',
      };

      it('should edit bookmarks', () => {
        return pactum
          .spec()
          .patch('/bookmarks/{id}')
          .withPathParams('id', '$S{bookmarkId}')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .withBody(editBookmark)
          .expectStatus(200)
          .expectBodyContains(bookmark.title)
          .expectBodyContains(bookmark.description)
          .expectBodyContains(editBookmark.link);
      });
    });

    describe('Delete bookmark', () => {
      it('should delete bookmark by id', () => {
        return pactum
          .spec()
          .delete('/bookmarks/{id}')
          .withPathParams('id', '$S{bookmarkId}')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(204);
      });

      it('Should get empty bookmarks', () => {
        return pactum
          .spec()
          .get('/bookmarks')
          .withHeaders({
            Authorization: `Bearer $S{token}`,
          })
          .expectStatus(200)
          .expectJsonLength(0);
      });
    });
  });
});
