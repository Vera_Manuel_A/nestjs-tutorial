import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { BookmarkModule } from './bookmark/bookmark.module';
import { PrismaModule } from './prisma/prisma.module';
import AuthModule from './auth/auth.module';

@Module({
  // decorators function add metada to function or class
  // main module to import any module
  imports: [ConfigModule.forRoot({ isGlobal: true }), AuthModule, UserModule, BookmarkModule, PrismaModule],
})
export class AppModule {}
