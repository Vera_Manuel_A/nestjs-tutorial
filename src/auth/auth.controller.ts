import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto } from './dto';

@Controller('auth')
class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  async signup(@Body() user: AuthDto) {
    return await this.authService.signup(user);
  }

  @HttpCode(HttpStatus.OK)
  @Post('signin')
  signin(@Body() user: AuthDto) {
    return this.authService.signin(user);
  }
}

export default AuthController;
